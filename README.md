eai-final-project

To start the api: 

Server Address: 35.211.150.9

1) From /opt/eai-final-project, run:
PORT=7000 bin/www &>/dev/null &

( Omit &>/dev/null & if you want to see the requests coming in, and want to cancel with CTRL-C )

Otherwise, must kill the api with:
kill -9 `pgrep node`
