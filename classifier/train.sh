#!/bin/bash
MODEL_DIR=./model
TRAIN_DIR=./train

if [ -d "$MODEL_DIR" ]; then
  rm -r $MODEL_DIR
fi

mkdir $MODEL_DIR

python3 retrain.py --image_dir $TRAIN_DIR --tfhub_module "https://tfhub.dev/google/imagenet/inception_v3/feature_vector/1" --output_graph $MODEL_DIR/output_graph.pb --output_labels $MODEL_DIR/output_labels.txt