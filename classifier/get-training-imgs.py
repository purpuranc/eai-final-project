###############################################
# Downloads training images from google images
# USAGE: python3 get-training-imgs.py
###############################################
from google_images_download import google_images_download
from os import listdir, rename, remove
from os.path import isfile, join, realpath, dirname
from shutil import rmtree
from platform import system
import uuid

imageDirectory = join(dirname(realpath(__file__)), "train")
downloader = google_images_download.googleimagesdownload()

models = []
limit = 200
chrome_driver = ""
plat = system().lower()

if plat == "windows":
    chrome_driver = "driver-win.exe"
elif plat == "darwin":
    chrome_driver = "driver-mac"
else:
    chrome_driver = "driver-linux"

chrome_driver = join(dirname(realpath(__file__)), "drivers", chrome_driver)

downloaderArguments = {
    "output_directory": imageDirectory, 
    "no_numbering": True, 
    "format": "jpg",
    "chromedriver": chrome_driver,
    "limit": limit
}



class Model:
    def __init__(self, name, search_terms):
        self.name = name
        self.search_terms = search_terms
    
    def getName(self):
        return self.name

    def getSearchTerms(self):
        return self.search_terms

def sanitize_image_names(path):
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
    for file in onlyfiles:
        rename(join(path,file), join(path,str(uuid.uuid1())+".jpg"))

def download_images_for_model(model):
    downloaderArguments['keywords'] = ','.join(model.getSearchTerms())
    downloaderArguments['image_directory'] = model.getName()
    downloader.download(downloaderArguments)

def delete_image_directory():
    try:
        rmtree(imageDirectory)
    except(OSError):
        pass
    
def register(model):
    models.append(model)

register(Model("gothic", ["gothic architecture"]))
register(Model("islamic", ["islamic architecture"]))
register(Model("modern", ["modern building architecture"]))
register(Model("japanese", ["japanese architecture"]))
register(Model("nordic", ["norse architecture"]))
register(Model("greek", ["greek architecture"]))


if __name__ == "__main__":
    delete_image_directory()
    for model in models:
        download_images_for_model(model)
        sanitize_image_names(join(imageDirectory, model.getName()))