var express = require('express');
var shell = require('shelljs');
var is_image_url = require('is-image-url')
var download_size = require('remote-file-size')
var download_file = require('download-file')
var uuid = require('uuid/v1')
const readChunk = require('read-chunk');
const isJpg = require('is-jpg');

const tempDir = '/tmp/eaifp'


var router = express.Router();

/* GET home page. */

const possible_labels = [
	'gothic',
	'japanese',
	'islamic',
	'modern',
	'nordic',
	'greek'
]



router.get('/', function(req, res, next) { 
	res.json("Use /api/some_image_url\n");
});

router.get('/:imageurl', function(req, res, next) { 
	
	var data = req.params;
	var url = data['imageurl']
	if(!is_image_url(url)) return res.status(403).send() //Only images
	download_size(url, (err, size) => {
		
		if(size > 20971520) return res.status(403).send() //20MB Max
		let filename = uuid()
		let options = {
			directory: tempDir,
			filename: filename+".jpg"
		}
		
		download_file(url, options, (err) => setTimeout(() => {
			
			if(err) return res.status(500).send()
			const buffer = readChunk.sync(tempDir+"/"+filename+".jpg", 0, 3)
			if(!isJpg(buffer)) return res.status(403).send()
			let command = "./classify.sh "+(tempDir+"/"+filename+".jpg")
			var output = shell.exec("cd classifier; "+command).stderr.split(/\n/)
			
			let list = []
			output.forEach(element => {
				let components = element.split(' ')
				if(components[0] == '') return
				let object = {}
				object['label'] = components[0]
				object['result'] = parseFloat(components[1])
				list.push(object)
			})

			list.sort((e1, e2) => {
				return e2['result'] - e1['result']
			})

			res.json({"labels": possible_labels, "features": list});
		}, 1000))
	})
});

module.exports = router;
